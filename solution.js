/**
 * @author Izzul Haq Al Hakam <izzul.haq.alhakam@gmail.com>
 * 
 * node version v10.16.3
 */

var express = require('express');
var path = require('path');

var indexPage = require('./index');
var app = express();

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use('/',indexPage);

const port = 3000;
app.listen(port, () => {
    console.log(`Listening on port ${port}!`
)})