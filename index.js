/**
 * @author Izzul Haq Al Hakam <izzul.haq.alhakam@gmail.com>
 * 
 * node version v10.16.3
 */

const express = require('express');
const router = express.Router();
const getData = require('./scraper');

// Get Bank Mega Promo Page
router.get('/', async function(req,res, next){
    const result = await getData();

    res.writeHead(200, {
        'Content-Type': 'text/plain'
    });
    res.write(JSON.stringify(result,null, 4));
    res.end();
})

module.exports = router;