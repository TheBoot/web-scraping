/**
 * @author Izzul Haq Al Hakam <izzul.haq.alhakam@gmail.com>
 * 
 * node version v10.16.3
 */

const cheerio = require('cheerio');
const axios = require('axios');
const fs = require('fs');

const siteUrl = 'https://www.bankmega.com/promolainnya.php';

// Sub Category based on product
// https://www.bankmega.com/ajax.promolainnya.php?product=0
// https://www.bankmega.com/ajax.promolainnya.php?product=0&subcat=1
// https://www.bankmega.com/ajax.promolainnya.php?product=0&subcat=1&page=1

// Get Data From URL
const fetchMainData = async () => {
    const result = await axios.get(siteUrl);
    return cheerio.load(result.data);
};

const fetchCatData = async (url) => {
    const result = await axios.get(url);
    return cheerio.load(result.data);
};

const fetchSubCatData = async (subUrl) => {
    const result = await axios.get(subUrl);
    return cheerio.load(result.data);
};

// Get Number Page 
const findTotalPage = function (str, string) {

    if (str.substring(0, 9) == string) {
        var page = str.substring(10, str.length);
        console.log("Page Amount = " + page);
        return parseInt(page);
    } else {
    }

}

// Get Page Length
async function getAmountPage(url) {

    var page = [];
    const $ = await fetchSubCatData(url);

    $('table.tablepaging').find('tbody > tr > td > a').each(function (index, element) {
        var str = $(element).attr("title")
        var totalPage = findTotalPage(str, "Page 1 of")
        if (Number.isInteger(totalPage)) {
            page.push(totalPage);
        }
    });

    return page;
}

// Write JSON Data to File
function writeJSONtoFile(jsonObject) {

    return new Promise((resolve, reject) => {
        fs.writeFile('solution.json', JSON.stringify(jsonObject, null, 4), (err, res) => {
            if (err) reject(err);
            resolve(res);
        })
    });

}

// Get Detail Image URL from detail Promo
async function getDetailImage(url) {

    const $ = await fetchSubCatData(url);
    return new Promise((resolve, reject) => {
        var detailImage = $('.keteranganinside').find('img').attr('src');
        // console.log("detail image : " + detailImage);
        resolve(detailImage);
    })

}

// Get Page Data 
async function getDataPage(url, page) {

    var mainUrl = "https://bankmega.com/"
    var data = [];
    var amountData = {}
    var categories

    for (var i = 1; i <= page; i++) {

        var urlwithPage = url + "&page=" + i;
        const $ = await fetchSubCatData(urlwithPage);

        categories = $('#subcatpromo').find('div[id="subcatselected"] > img').attr("title");

        $('ul[id="promolain"]').find('li > a').each(async function (index, element) {
            var promo_detail = $(element).attr("href");
            var promo_title = $(element).find('img').attr("title");
            var imageurl = $(element).find('img').attr("src");

            // Get Image on detail promo
            var promo_image = await getDetailImage(mainUrl + promo_detail)

            data.push({ promo_link: mainUrl + promo_detail, title: promo_title, imageUrl: mainUrl + imageurl, imageDetailUrl: mainUrl + promo_image });

        });

        // Create JSON Here for each category
        if (categories != undefined) {
            amountData[categories] = data;
        } else {
            amountData["all_promo"] = data;
        }
    }

    return (amountData);
}

// Main Get All Page Data
const getData = async () => {

    var objectJMega = {}; 
    var numOfProduct = 0;
    var product = [];
    var catUrl = "";

    // Get Main Data 
    const $ = await fetchMainData();

    // Product Data
    $("#contentpromolain2 ").find('div > div').each(function (index, element) {

        if ($(element).attr("id") != undefined && $(element).attr("id") != "subcatpromo" && $(element).attr("id") != "promolain_inside") {
            product.push($(element).attr("id"));
            // console.log($(element).attr("id"));
            numOfProduct += 1;
        }
    });

    // Get Sub Category
    for (var i = 1; i <= numOfProduct; i++) {
       
        catUrl = (siteUrl + "?product=" + i);
        var arrproduct = product[i - 1];

        // Credit Card Product
        if (arrproduct == "kartukredit") {
            var subCatUrl = [];
            var collectData = [];
            var numOfCat = 1;
            console.log(catUrl);

            const $ = await fetchCatData(catUrl);
            
            // Sub Category Promo
            $('#subcatpromo').find('img').each(async function (index, element) {
                numOfCat = index + 1;
                // Sub Category URL
                subCatUrl.push(catUrl + "&subcat=" + numOfCat);
            });
            
            for (var j = 0; j < subCatUrl.length; j++) {

                // Get Amount Page
                var page = await getAmountPage(subCatUrl[j]);
                // Get Data From Each Page
                var data = await getDataPage(subCatUrl[j], page)
                collectData.push(data);

            }

            objectJMega[arrproduct] = collectData;

        } 
        else { // Other Product
            
            var collectData = [];
            console.log(catUrl);
            
            // Get Amount Page
            var page = await getAmountPage(catUrl);
            // Get Data From Each Page
            var data = await getDataPage(catUrl, page)
            collectData.push(data);

            objectJMega[arrproduct] = collectData;
            
        }
    }

    // Write Data to file solution.js
    await writeJSONtoFile(objectJMega)

    // Show Data to Browser localhost:3000
    return objectJMega;
};

module.exports = getData;